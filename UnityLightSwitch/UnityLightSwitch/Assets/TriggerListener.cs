﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// these are name spaces: Locations of code that we need

// this is a class taht inherits from MonoBehaviour
public class TriggerListener : MonoBehaviour
{
    //At the top of classes we declare our variables
    public bool PlayerEntered = false;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Start was called");
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update was called");
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerEntered = true;
        }
    }

    //Checks if player exits trigger
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerEntered = false;
        }
    }

}