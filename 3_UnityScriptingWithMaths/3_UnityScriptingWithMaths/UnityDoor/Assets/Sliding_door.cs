﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sliding_door : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float moveAmount = 1f;
    public float snap = 0.02f;
    public float speed = 5f;
    public AudioSource audioSource;


    // Use this for initialization
    void Start ()
    {
        audioSource = this.GetComponent<AudioSource>();

    }

    //open door
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {         
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", moveAmount);
            audioSource.Stop();
            audioSource.Play();
            Debug.Log("Audio Plays");
        }
    }

    //close door
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    IEnumerator DoorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        Debug.Log("Started");
        while(xPos < (target - 0.02f) || xPos > (target + snap))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Moving");
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Finished");
        yield return null;
    }
}
